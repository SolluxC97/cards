// Lab Exercise 2
// Ross Karvonen

# include <iostream>
# include <conio.h>
# include <string>

using namespace std;

enum Rank
{
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};

enum Suit
{
	Hearts,
	Diamonds,
	Spades,
	Clubs
};

struct Card
{
	Suit Suit;
	Rank Rank;
};

void PrintCard(Card card) {
	string suit = "";
	switch (card.Suit)
	{
	case 0: suit = "Hearts";
	case 1: suit = "Diamonds";
	case 2: suit = "Spades";
	case 3: suit = "Clubs";
	default: suit = "Wut?";
	}
	string rank = "";
	switch (card.Rank)
	{
	case 2: rank = "Two";
	case 3: rank = "Three";
	case 4: rank = "Four";
	case 5: rank = "Five";
	case 6: rank = "Six";
	case 7: rank = "Seven";
	case 8: rank = "Eight";
	case 9: rank = "Nine";
	case 10: rank = "Ten";
	case 11: rank = "Jack";
	case 12: rank = "Queen";
	case 13: rank = "King";
	case 14: rank = "Ace";
	}

	cout << rank << " of " << suit << "/n";
}

Card HighCard(Card card1, Card card2) {
	if (card1.Rank > card2.Rank) {
		return card1;
	} else if (card1.Rank == card2.Rank) {
		return card1;
	} else {
		return card2;
	}
}

int main()
{
	_getch();
	return 0;
}